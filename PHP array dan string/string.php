<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih string PHP</h1>
    <?php
    echo "<h3>Soal No 1</h3>";
    $first_sentence = "Hello PHP" ;
    echo "Kata Pertama: ". $first_sentence. "<br>";
    echo "Panjang String: ". strlen("$first_sentence"). "<br>";
    echo "Jumlah Kata: ". str_word_count("$first_sentence"). "<br><br>";
    
    $second_sentence ="I'm Ready For The Challenges" ;
    echo "Kata Kedua: ". $second_sentence. "<br>";
    echo "Panjang String: ". strlen("$second_sentence"). "<br>";
    echo "Jumlah Kata: ". str_word_count("$second_sentence"). "<br>";

    echo "<h3>Soal No 2</h3>";
    $string = "I Love PHP";
    echo "String : " . $string. "<br>";
    echo "Kata Pertama : ". substr($string,0,1). "<br>";
    echo "Kata Kedua : ". substr($string,2,4). "<br>";
    echo "Kata Ketiga : ". substr($string,7,3). "<br>";

    echo "<h3>Soal No 3</h3>";
    $string2 = "PHP is Old But Sexy!";
    echo "string : ". $string2. "<br>";
    echo "Output : ". str_replace("Sexy", "Awesome", $string2);

    ?>
</body>
</html>