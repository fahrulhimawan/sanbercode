@extends('layouts.master')

@section('title')

    Halaman edit Bio

@endsection

@section('content')
<form action="/cast/{{$cast->id}} " method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama</label>
      <input type="text" value="{{$cast->nama}}" name="nama" class="form-control" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="text" value="{{$cast->umur}}" name="umur" class="form-control" >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10" >{{$cast->bio}}"</textarea>   
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection