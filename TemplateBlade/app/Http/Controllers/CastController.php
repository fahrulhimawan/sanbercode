<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    } //
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio'  => 'required',
        ],
        [
            'nama.required' => 'Nama tidak boleh kosong!',
            'nama.min' => 'Nama minimal 5 karakter!',
            'umur.required' => 'Umur tidak boleh kosong!',
            'bio.required' => 'Bio tidak boleh kosong' ,
        ]
    );
    DB::table('cast')->insert([
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
    ]);
        return redirect('/cast');
    }
    public function index()
    {
        $data = DB::table('cast')->get();
        

        return view('cast.tampil', ['data' => $data]);
    }
    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);

    }
    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio'  => 'required',
        ],
        [
            'nama.required' => 'Nama tidak boleh kosong!',
            'nama.min' => 'Nama minimal 5 karakter!',
            'umur.required' => 'Umur tidak boleh kosong!',
            'bio.required' => 'Bio tidak boleh kosong' ,
        ]);

        $affected = DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
            ]);
        
        return redirect('/cast');
    }
    public function destroy($id)
    {
        $deleted = DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');

    }
}
