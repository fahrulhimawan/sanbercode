<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function home()
    {
        return view('welcome');
    }
    public function diagram()
    {
        return view('table');
    }    

}
