<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[TableController::class,'home']);
Route::get('/table',[TableController::class,'diagram']);
Route::get('/data-tables', function(){
    return view('data');
});


//CRUD Cast
//Create Data
//mengarah ke form tambah data
Route::get('/cast/create',[CastController::class, 'create']);
//menyimpan data ke table kategori db
Route::post('/cast',[CastController::class, 'store']);
//mengambil data di db
Route::get('/cast',[CastController::class, 'index']);
//detail cast ambil berdasarkan id
Route::get('/cast/{cast_id}',[CastController::class, 'show']);
//Update Data
Route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);
//update data berdasarkan id
Route::put('/cast/{cast_id}',[CastController::class, 'update']);
//Delate Data
Route::delete('/cast/{cast_id}',[CastController::class, 'destroy']);