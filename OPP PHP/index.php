<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal ("shaun");

echo "Name : $sheep->name <br>";
echo "Legs : $sheep->legs <br>";
echo "cold blooded : $sheep->cold_blooded<br><br>";

$frog = new Frog("buduk");

echo "Name : $frog->name <br>";
echo "Legs : $frog->legs <br>";
echo "cold blooded : $frog->cold_blooded<br>"; 
echo $frog->jump() .  "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "cold blooded : $sungokong->cold_blooded<br>"; 
$sungokong->yell();