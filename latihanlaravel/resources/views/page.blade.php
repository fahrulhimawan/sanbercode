<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru!</h1>	
<h3>Sign Up Form</h3>

<form action="/send" method="POST">
@csrf

	<label>First name:</label><br><br>
	<input type="text" name="first"><br><br>
	<label>Last name:</label><br><br>
	<input type="text" name="last"><br><br>
	<label>Gender:</label><br><br>
	<input type="radio" name="Gender">Male<br>
	<input type="radio" name="Gender">Female<br>
	<input type="radio" name="Gender">Other<br>
	<label>Nationality:</label><br><br>
	<select name="Nationality">
		<option value="Indonesian">Indonesian</option>
		<option value="Singapure">Singapure</option>
		<option value="Malaysian">Malaysian</option>
		<option value="Australian">Australian</option>
	</select><br><br>
	<label>Language Spoken:</label><br><br>
	<input type="checkbox" name="Language Speakers">Bahasa Indonesia<br>
	<input type="checkbox" name="Language Speakers">English<br>
	<input type="checkbox" name="Language Speakers">Other<br><br>
	<label>Bio:</label><br><br>
	<textarea name="message" rows="10" cols="30"></textarea><br><br>
	<input type="submit" name="submit" value="Sign Up">
</form>
    
</body>
</html>
