<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page');
    }
    public function selamatDatang(Request $request)
    {
        $firstName = $request ['first'];
        $lastName = $request['last'];
        return view('welcome', ['firstName' => $firstName, 'lastName' => $lastName]);
    }
}
